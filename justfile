_build := "wasm-pack build --target web"

build *ARGS:
	{{ _build }} {{ ARGS }}
watch *ARGS:
	cargo watch -i .gitignore -i "pkg/*" -s "{{ _build }} {{ ARGS }}"
serve *ARGS:
	python -m http.server {{ ARGS }}

