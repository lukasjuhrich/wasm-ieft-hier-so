import init, { State, add_pending_circle, update_pending_circle, commit_circle, init_state, render, handle_key } from "./pkg/wasm_ieft_hier_so.js"
await init();

/** @type {State} */
let state = init_state();

function registerClickHandler(_) {
  /** @type {HTMLCanvasElement} */
  const canvas = document.getElementById("canvas");
  canvas.addEventListener("mousedown", e => {
    state = add_pending_circle(e.offsetX, e.offsetY, state)
    render(state)
  })
  canvas.addEventListener("mousemove", e => {
    state = update_pending_circle(e.offsetX, e.offsetY, state)
    render(state)
  })
  canvas.addEventListener("mouseup", _ => {
    state = commit_circle(state)
    render(state)
  })
  window.addEventListener("keypress", e => {
    state = handle_key(e, state)
    render(state)
  })
  window.addEventListener("resize", _ => setCanvasSize(canvas))
  setCanvasSize(canvas)
}

/**
*@param {HTMLCanvasElement} canvas 
*/
function setCanvasSize(canvas) {
  console.debug("Setting canvas width / height:", canvas.clientWidth, canvas.clientHeight)
  canvas.width = canvas.clientWidth
  canvas.height = canvas.clientHeight
  render(state)
}

if (document.readyState === "complete") {
  registerClickHandler()
} else {
  document.addEventListener("DOMContentLoaded", registerClickHandler)
}
add_pending_circle(0, 0, state);

