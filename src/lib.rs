use std::f64::consts::TAU;

use log;
use wasm_bindgen::prelude::*;
use wasm_logger as wl;
use web_sys::{window, CanvasRenderingContext2d, HtmlCanvasElement, KeyboardEvent};

#[wasm_bindgen(start)]
pub fn main() -> () {
    wl::init(wl::Config::new(log::Level::Trace));
}

fn get_canvas(canvas_id: &str) -> HtmlCanvasElement {
    let w = window().expect("`window` not found");
    let document = w.document().expect("`document` not found");
    let canvas = document
        .get_element_by_id(canvas_id)
        .unwrap_or_else(|| panic!("canvas#{} not found", canvas_id));
    let canvas = canvas.dyn_into::<HtmlCanvasElement>().unwrap_or_else(|el| {
        panic!(
            "Element of id {} is not <canvas>, it is {}",
            canvas_id,
            el.tag_name()
        )
    });
    canvas
}

pub fn get_ctx(canvas: &HtmlCanvasElement) -> Result<CanvasRenderingContext2d, JsValue> {
    let ctx = canvas
        .get_context("2d")?
        .expect("Could not get `2d` rendering context")
        .dyn_into::<CanvasRenderingContext2d>()
        .expect("Did not get `CanvasRenderingContext2d`");

    Ok(ctx)
}

#[wasm_bindgen]
#[derive(Clone, Copy, Debug)]
pub struct Circle {
    pub x: f64,
    pub y: f64,
    pub r: f64,
}

#[wasm_bindgen]
#[derive(Clone, Debug, Default)]
pub struct State {
    pending_circle: Option<Circle>,
    circles: Vec<Circle>,
}

impl State {
    fn iter_drawables(&self) -> impl Iterator<Item = &Circle> {
        self.circles.iter().chain(self.pending_circle.iter())
    }
}

#[wasm_bindgen]
pub fn init_state() -> State {
    State::default()
}

#[wasm_bindgen]
pub fn add_pending_circle(x: f64, y: f64, state: &State) -> State {
    let pending = Circle { x, y, r: 50.0 };
    log::trace!("adding pending {:?}", pending);
    State {
        pending_circle: Some(pending),
        ..state.clone()
    }
}

#[wasm_bindgen]
pub fn update_pending_circle(mousex: f64, mousey: f64, state: &State) -> State {
    if let Some(old_pending) = state.pending_circle {
        let new_pending = Circle {
            x: old_pending.x,
            y: old_pending.y,
            r: ((old_pending.x - mousex).powi(2) + (old_pending.y - mousey).powi(2)).sqrt(),
        };
        State {
            pending_circle: Some(new_pending),
            ..state.clone()
        }
    } else {
        state.clone()
    }
}

#[wasm_bindgen]
pub fn commit_circle(state: &State) -> State {
    if let Some(pending) = state.pending_circle {
        log::trace!("committing pending circle {:?}", pending);
        let mut new_circles = state.circles.clone();
        new_circles.push(pending);
        State {
            circles: new_circles,
            pending_circle: None,
            ..state.clone()
        }
    } else {
        state.clone()
    }
}

fn render_circle(circle: Circle, ctx: &CanvasRenderingContext2d) -> Result<(), JsValue> {
    ctx.begin_path();
    ctx.arc(circle.x, circle.y, circle.r, 0.0, TAU)?;
    ctx.stroke();
    Ok(())
}

#[wasm_bindgen]
pub fn render(state: &State) -> Result<(), JsValue> {
    let canvas = get_canvas("canvas");
    let ctx = get_ctx(&canvas)?;
    ctx.clear_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);

    for &circle in state.iter_drawables() {
        render_circle(circle, &ctx)?;
    }

    Ok(())
}

#[wasm_bindgen]
pub fn handle_key(ke: KeyboardEvent, state: &State) -> State {
    let key = ke.key();
    log::trace!("Got key press: {:?}", key);
    State {
        circles: state
            .circles
            .split_last()
            .map_or(vec![], |(_, rest)| rest.into()),
        ..state.clone()
    }
}
